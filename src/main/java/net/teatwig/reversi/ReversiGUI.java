package net.teatwig.reversi;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import javax.swing.*;

public class ReversiGUI extends JFrame {

    private final Reversi r;
    private JPanel jPanelGameboard, jPanelControls;
    private final JLabel[][] box;
    private JButton jButtonSubmit;
    private JTextField jTextFieldX, jTextFieldY;
    private JTextPane jTextPane;
    private final int FIELD_SIZE = 50;
    private JSplitPane jSplitPane;

    /**
     * Creates new form GameDynGUI
     */
    public ReversiGUI() {
        r = new Reversi();

        box = new JLabel[r.getGameboardWidth()][r.getGameboardHeight()];

        initComponents();

        updateBoard();

        jTextFieldX.grabFocus();
    }

    private void updateBoard() {
        for (int j = 0; j < r.getGameboardHeight(); j++) {
            for (int i = 0; i < r.getGameboardWidth(); i++) {
                if (r.getGamefield(i, j) == r.WHITE) { // Apple
                    box[i][j].setBackground(Color.WHITE);
                } else if (r.getGamefield(i, j) == r.BLACK) {
                    box[i][j].setBackground(Color.BLACK);
                } else {
                    box[i][j].setBackground(new java.awt.Color(0, 128, 0));
                }
            }
        }

        updateGameConsole();
    }

    private void updateGameConsole() {
        jTextPane.setText("");
        if (r.blacksTurn) {
            jTextPane.setText("Blacks turn.\n");
        }
        if (r.whitesTurn) {
            jTextPane.setText("Whites turn.\n");
        }
        if (r.getCurrentErrorMessages() != null) {
            jTextPane.setText(jTextPane.getText() + r.getCurrentErrorMessages());
        }
    }

    private void initComponents() {
        jSplitPane = new JSplitPane();

        jPanelGameboard = new JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Reversi");

//        jPanel.setBackground(new java.awt.Color(255, 0, 255));
        jPanelGameboard.setPreferredSize(new java.awt.Dimension(r.getGameboardWidth() * FIELD_SIZE, r.getGameboardHeight() * FIELD_SIZE));
        jPanelGameboard.setFocusable(true);
        jPanelGameboard.requestFocus();
        jPanelGameboard.setLayout(new GridLayout(r.getGameboardHeight(), r.getGameboardWidth()));

        // Initializes game field
        for (int j = 0; j < r.getGameboardHeight(); j++) {
            for (int i = 0; i < r.getGameboardWidth(); i++) {
                int ni = i + 1;
                int nj = j + 1;
                box[i][j] = new JLabel(ni + "," + nj);
                box[i][j].setOpaque(true);
                box[i][j].setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
                //box[i][j].setBounds(i*STD, j*STD, STD, STD);

                jPanelGameboard.add(box[i][j]);
            }
        }

        jPanelControls = new JPanel();

        jTextFieldX = new javax.swing.JTextField();
        jTextFieldX.setText("x-coordinate");
        jTextFieldX.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                jTextFieldX.selectAll();
            }

            @Override
            public void focusLost(FocusEvent e) {
            }
        });
        jTextFieldX.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                enterPressed(evt);
            }
        });

        jTextFieldY = new javax.swing.JTextField();
        jTextFieldY.setText("y-coordinate");
        jTextFieldY.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                jTextFieldY.selectAll();
            }

            @Override
            public void focusLost(FocusEvent e) {
            }
        });
        jTextFieldY.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                enterPressed(evt);
            }
        });

        jButtonSubmit = new javax.swing.JButton();
        jButtonSubmit.setText("Submit");
        jButtonSubmit.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSubmitActionPerformed(evt);
            }
        });
        jButtonSubmit.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                enterPressed(evt);
            }
        });

        jTextPane = new JTextPane();
        jTextPane.setText("");
        jTextPane.setEditable(false);
        jTextPane.setEnabled(false);

        jPanelControls.add(jTextFieldX);
        jPanelControls.add(jTextFieldY);
        jPanelControls.add(jButtonSubmit);
        jPanelControls.add(jTextPane);

        jSplitPane.setTopComponent(jPanelGameboard);
        jSplitPane.setBottomComponent(jPanelControls);
        jSplitPane.setDividerSize(0);
        jSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);

        getContentPane().add(jSplitPane);

        pack();
    }

    private void enterPressed(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            transferInputToGame();
        }
    }

    private void jButtonSubmitActionPerformed(java.awt.event.ActionEvent evt) {
        transferInputToGame();
    }

    protected void transferInputToGame() {
        int posX = 0;
        int posY = 0;
        try {
            posX = Integer.valueOf(jTextFieldX.getText()) - 1;
            posY = Integer.valueOf(jTextFieldY.getText()) - 1;
        } catch (NumberFormatException nfe) {
            jTextPane.setText(jTextPane.getText() + "Please enter only numbers into the Textfields.");
        }

        r.placeDiscOnPosAndCheckIfPosIsValid(posX, posY);
        updateBoard();

        jTextFieldX.grabFocus();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ReversiGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ReversiGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ReversiGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ReversiGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ReversiGUI().setVisible(true);
            }
        });
    }
}
