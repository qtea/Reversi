package net.teatwig.reversi;

public class Reversi {

    private final int[][] gameboard;
    private final int GAMEBOARD_HEIGHT, GAMEBOARD_WIDTH;
    protected int EMPTY = 0;
    protected int WHITE = 1;
    protected int BLACK = 2;
    protected boolean noRecursionYet;
    protected Direction NORTHWEST, NORTH, NORTHEAST, EAST, SOUTHEAST, SOUTH, SOUTHWEST, WEST;
    public boolean gameIsRunning;
    public boolean whitesTurn, blacksTurn;
    private String currentErrorMessages;

    public Reversi() {
        GAMEBOARD_HEIGHT = GAMEBOARD_WIDTH = 8;
        gameboard = new int[8][8];
        initDirections();

        gameIsRunning = true;
        whitesTurn = true;
        blacksTurn = false;
        resetGameboard();
    }

    private void initDirections() {
        NORTHWEST = new Direction(-1, -1);
        NORTH = new Direction(0, -1);
        NORTHEAST = new Direction(1, -1);
        EAST = new Direction(1, 0);
        SOUTHEAST = new Direction(1, 1);
        SOUTH = new Direction(0, 1);
        SOUTHWEST = new Direction(-1, 1);
        WEST = new Direction(-1, 0);
    }

    public void resetGameboard() {
        emptyGameboard();
        placeDiscsOnStartingPosition();

    }

    protected void placeDiscsOnStartingPosition() {
        gameboard[GAMEBOARD_HEIGHT / 2 - 1][GAMEBOARD_WIDTH / 2 - 1] = 1;
        gameboard[GAMEBOARD_HEIGHT / 2 - 1][GAMEBOARD_WIDTH / 2] = 2;
        gameboard[GAMEBOARD_HEIGHT / 2][GAMEBOARD_WIDTH / 2 - 1] = 2;
        gameboard[GAMEBOARD_HEIGHT / 2][GAMEBOARD_WIDTH / 2] = 1;
    }

    public void emptyGameboard() {
        for (int i = 0; i < GAMEBOARD_HEIGHT; i++) {
            for (int j = 0; j < GAMEBOARD_WIDTH; j++) {
                gameboard[i][j] = EMPTY;
            }
        }
    }

    public void placeDiscOnPosAndCheckIfPosIsValid(int posX, int posY) {
        currentErrorMessages = "";
        
        if (posIsNotInBounds(posX, posY)) {
            currentErrorMessages += "ERR: Please enter only numbers between 1 and " + GAMEBOARD_WIDTH + " for x and 1 and " + GAMEBOARD_HEIGHT + " for y.\n";
        } else {
            if (posIsValidAndNotOccupied(posX, posY)) {
                placeDiscOnPos(posX, posY);
                changePlayerTurn();
            }
        }

    }

    protected boolean posIsValidAndNotOccupied(int posX, int posY) {
        return posIsNotOccupied(posX, posY) && posIsValid(posX, posY);
    }

    protected boolean posIsNotInBounds(int posX, int posY) {
        return posX < 0 || posX >= GAMEBOARD_WIDTH || posY < 0 || posY >= GAMEBOARD_HEIGHT;
    }

    protected boolean posIsNotOccupied(int posX, int posY) {
        if (gameboard[posY][posX] == EMPTY) {
            return true;
        } else {
            return false;
        }
    }

    protected boolean posIsValid(int posX, int posY) {
        boolean b = false;
        noRecursionYet = true;
        if (sameColorDiscInDirection(NORTHWEST, posX, posY)) {
            b = true;
        }
        if (sameColorDiscInDirection(NORTH, posX, posY)) {
            b = true;
        }
        if (sameColorDiscInDirection(NORTHEAST, posX, posY)) {
            b = true;
        }
        if (sameColorDiscInDirection(EAST, posX, posY)) {
            b = true;
        }
        if (sameColorDiscInDirection(SOUTHEAST, posX, posY)) {
            b = true;
        }
        if (sameColorDiscInDirection(SOUTH, posX, posY)) {
            b = true;
        }
        if (sameColorDiscInDirection(SOUTHWEST, posX, posY)) {
            b = true;
        }
        if (sameColorDiscInDirection(WEST, posX, posY)) {
            b = true;
        }
        return b;
    }

    protected boolean sameColorDiscInDirection(Direction direction, int posX, int posY) {
        boolean b = false;

        posX += direction.getX();
        posY += direction.getY();

        if (posIsInBounds(posX, posY)) {
            if (noRecursionYet) {
                noRecursionYet = false;
                if (gameboard[posY][posX] == oppositePlayerColor()) {
                    b = sameColorDiscInDirection(direction, posX, posY);
                    if (b == true) {
                        changeDiscToOwnColor(posX, posY);
                    }
                } else {
                    noRecursionYet = true;
                    b = false;
                }
            } else {
                if (gameboard[posY][posX] == currentPlayerColor()) {
                    noRecursionYet = true;

                    b = true;
                } else if (gameboard[posY][posX] == oppositePlayerColor()) {
                    b = sameColorDiscInDirection(direction, posX, posY);
                    if (b == true) {
                        changeDiscToOwnColor(posX, posY);
                    }
                } else {
                    noRecursionYet = true;
                    b = false;
                }

            }
        } else {
            noRecursionYet = true;
            b = false;

        }
        return b;
    }

    public void changeDiscToOwnColor(int posX, int posY) {
        gameboard[posY][posX] = currentPlayerColor();
    }

    protected boolean posIsInBounds(int posX, int posY) {
        return posX >= 0 && posX < GAMEBOARD_WIDTH && posY >= 0 && posY < GAMEBOARD_HEIGHT;
    }

    protected void placeDiscOnPos(int posX, int posY) {
        if (blacksTurn) {
            gameboard[posY][posX] = BLACK;
        } else if (whitesTurn) {
            gameboard[posY][posX] = WHITE;
        }
    }

    public void changePlayerTurn() {
        if (blacksTurn) {
            whitesTurn = true;
            blacksTurn = false;
        } else if (whitesTurn) {
            blacksTurn = true;
            whitesTurn = false;
        }
    }

    public int currentPlayerColor() {
        if (blacksTurn) {
            return BLACK;
        } else if (whitesTurn) {
            return WHITE;
        } else {
            currentErrorMessages += "ERR: Current Player Color is neither black or white.\n";
            return 0;
        }
    }

    public int oppositePlayerColor() {
        if (blacksTurn) {
            return WHITE;
        } else if (whitesTurn) {
            return BLACK;
        } else {
            currentErrorMessages += "ERR: Opposite Player Color is neither black or white.\n";
            return 0;
        }
    }

    public int[][] getGameboard() {
        return gameboard;
    }

    public int getGamefield(int xPos, int yPos) {
        return gameboard[yPos][xPos];
    }

    public int getGameboardHeight() {
        return GAMEBOARD_HEIGHT;
    }

    public int getGameboardWidth() {
        return GAMEBOARD_WIDTH;
    }
    
    public String getCurrentErrorMessages(){
        return currentErrorMessages;
    }
}
