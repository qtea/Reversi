package net.teatwig.reversi;

public class Direction {

    int xOffset, yOffset;

    public Direction(int pXOffset, int pYOffset) {
        xOffset = pXOffset;
        yOffset = pYOffset;
    }

    public int getX() {
        return xOffset;
    }

    public int getY() {
        return yOffset;
    }
}
